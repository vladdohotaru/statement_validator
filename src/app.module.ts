import { Module } from '@nestjs/common';
import { BankingStatementController } from './banking-statement.controller';
import { BankingStatementService } from './banking-statement.service';
import { XmlParserService } from './xml-parser.service';
import { CsvParserService } from './csv-parser.service';
import { StatementValidatorService } from './statement-validator.service';

@Module({
  imports: [],
  controllers: [BankingStatementController],
  providers: [BankingStatementService, XmlParserService, CsvParserService, StatementValidatorService],
})
export class AppModule {}
