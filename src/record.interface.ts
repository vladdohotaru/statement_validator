export interface Record {
    reference: string;
    accountNumber: string;
    description: string;
    startBalance: number;
    mutation: string;
    endBalance: number;
}