import { Injectable } from '@nestjs/common';
import { parseStringPromise } from 'xml2js';
import { Record } from './record.interface';

@Injectable()
export class XmlParserService {
  async parseXml(xmlContent: string): Promise<Record[]> {
    const parsedXml: any = await parseStringPromise(xmlContent, {
        explicitArray: false,
        mergeAttrs: true,
      });
      console.log('parsedXml', parsedXml)
      const records: Record[] = parsedXml.records.record;
      return Array.isArray(records) ? records : [records];
  }
}