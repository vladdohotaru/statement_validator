export interface ValidationResult {
    message: string;
    errors: string[];
  }
  