import { Controller, Post, UploadedFile, UseInterceptors, HttpException, HttpStatus } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { BankingStatementService } from './banking-statement.service';
import { ValidationResult } from './response.interface';

@Controller('banking')
export class BankingStatementController {
  constructor(private readonly bankingStatementService: BankingStatementService) {}

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: any): Promise<string[] | ValidationResult> {
    const fileType = this.getFileType(file.originalname);

    if (fileType !== 'xml' && fileType !== 'csv') {
      throw new HttpException('Invalid file type. Supported types are XML and CSV.', HttpStatus.BAD_REQUEST);
    }

    const fileContent = file.buffer.toString();
    const failedRecords = await this.bankingStatementService.validateStatementFile(fileContent, fileType);

    if (failedRecords.length > 0) {
      const response: ValidationResult = { message: 'Validation failed', errors: failedRecords };
      throw new HttpException(response, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    return [ 'Validation successful' ];
  }

  private getFileType(filename: string): string {
    const parts = filename.split('.');
    return parts[parts.length - 1].toLowerCase();
  }
}
