import { Injectable } from '@nestjs/common';
import * as csvParser from 'csv-parser';
import { Readable } from 'stream';
import { Record } from './record.interface';

@Injectable()
export class CsvParserService {
  async parseCsv(csvContent: string): Promise<Record[]> {
    return new Promise((resolve, reject) => {
        const records: Record[] = [];
        Readable.from(csvContent)
          .pipe(csvParser())
          .on('data', (data) => {
            const record: Record = {
              reference: data.Reference,
              accountNumber: data['Account Number'],
              description: data.Description,
              startBalance: parseFloat(data['Start Balance']),
              mutation: data.Mutation,
              endBalance: parseFloat(data['End Balance']),
            };
            records.push(record);
          })
          .on('end', () => {
            resolve(records);
          })
          .on('error', (error) => {
            reject(error);
          });
      });
    }
}