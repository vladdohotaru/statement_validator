import { Injectable } from '@nestjs/common';
import { XmlParserService } from './xml-parser.service';
import { CsvParserService } from './csv-parser.service';
import { StatementValidatorService } from './statement-validator.service';

@Injectable()
export class BankingStatementService {
  constructor(
    private readonly xmlParserService: XmlParserService,
    private readonly csvParserService: CsvParserService,
    private readonly statementValidatorService: StatementValidatorService,
  ) {}

  async validateStatementFile(fileContent: string, fileType: string): Promise<string[]> {
    let records: any[];

    if (fileType === 'xml') {
      records = await this.xmlParserService.parseXml(fileContent);
    } else if (fileType === 'csv') {
      records = await this.csvParserService.parseCsv(fileContent);
    } else {
      throw new Error('Unsupported file type');
    }

    return this.statementValidatorService.validateStatement(records);
  }
}
