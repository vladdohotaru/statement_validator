import { Injectable } from '@nestjs/common';

@Injectable()
export class StatementValidatorService {
  validateStatement(records: any[]): string[] {
    const referenceSet = new Set<string>();
    const failedRecords: string[] = [];

    for (const record of records) {
      if (referenceSet.has(record.reference)) {
        failedRecords.push(`Duplicate reference: ${record.reference}`);
      }

      const parsedMutation = parseFloat(record.mutation);
      const calculatedEndBalance: number = parseFloat(record.startBalance) + parsedMutation;

      if (Math.abs(calculatedEndBalance - record.endBalance) > 0.01) {
        failedRecords.push(
          `Incorrect endBalance for reference ${record.reference}. Expected: ${calculatedEndBalance}, Got: ${record.endBalance}`,
        );
      }

      referenceSet.add(record.reference);
    }

    return failedRecords;
  }
}
